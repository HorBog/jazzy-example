//
//  Mathematician.swift
//  JazzyTest
//
//  Created by Bohdan Hordiienko on 6/7/18.
//  Copyright © 2018 Bohdan Hordiienko. All rights reserved.
//

import Foundation
import UIKit

struct Mathematician {
    let fullName: String
    let photo: UIImage
}
