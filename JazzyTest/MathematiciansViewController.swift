//
//  MathematiciansViewController.swift.swift
//  JazzyTest
//
//  Created by Bohdan Hordiienko on 6/7/18.
//  Copyright © 2018 Bohdan Hordiienko. All rights reserved.
//

import UIKit

///This class is created for shovind mathematicians
class MathematiciansViewController: UIViewController {
    
    @IBOutlet weak var mathematicianPhoto: UIImageView!
    @IBOutlet weak var mathematicianFullName: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    /**
     Pointer on selected mathematician
     */
    var selectedPerson = 0
    /**
     Array of Mathematician
 */
    var mathematicians = [Mathematician(fullName: "Leonardo Fibonacci", photo: #imageLiteral(resourceName: "fibonacci")),
                          Mathematician(fullName: "Pythagoras of Samos", photo: #imageLiteral(resourceName: "pythagoras")),
                          Mathematician(fullName: "Leonhard Euler", photo: #imageLiteral(resourceName: "euler")),
                          Mathematician(fullName: "Euclid", photo:#imageLiteral(resourceName: "evklid")),
                          Mathematician(fullName: "Blaise Pascal", photo: #imageLiteral(resourceName: "pascal"))]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setupView(){
        showMathematician(selected: selectedPerson)
        previousButton.isHidden = true
    }
    /**
     Action hendler of `nextButton`
     * increases `selectedPerson` on 1
     * calls function `showMathematician(_:)`
     * hides `nextButton` if `selectedPerson` is equal to mathematicians.count
     */
    @IBAction func showNext(_ sender: Any) {
        selectedPerson += 1
        showMathematician(selected: selectedPerson)
        nextButton.isHidden = selectedPerson == mathematicians.count - 1 ? true : false
        if selectedPerson == 1 {
            previousButton.isHidden = false
        }
    }
    /**
    Action hendler of `previousButton`
    * decreases `selectedPerson` on 1
    * calls function `showMathematician(_:)`
    * hides `previousButton` if `selectedPerson` is equal to 0
   */
    @IBAction func showPrevious(_ sender: Any) {
        selectedPerson -= 1
        showMathematician(selected: selectedPerson)
        previousButton.isHidden = selectedPerson == 0 ? true : false
        if selectedPerson == mathematicians.count - 2 {
            nextButton.isHidden = false
        }
    }
    
    /**
     Call this function for showing chosen mathematician.
     
     - parameter selectedPerson : position in the array 'mathematicians'
     - returns : Nothing
     ### Usage Example: ###
     ````
     showMathematician(selected: 2)
     ````
     */
    
    func showMathematician(selected: Int){
        mathematicianPhoto.image = mathematicians[selected].photo
        mathematicianFullName.text = mathematicians[selected].fullName
    }
}
